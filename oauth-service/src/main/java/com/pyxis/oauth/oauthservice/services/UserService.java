package com.pyxis.oauth.oauthservice.services;

import java.util.List;
import java.util.stream.Collectors;

import com.pyxis.common.users.commonuser.models.entity.SystemUser;
import com.pyxis.oauth.oauthservice.clients.UserFeingClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import brave.Tracer;
import feign.FeignException;

@Service
public class UserService implements IUserService, UserDetailsService {

    private Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserFeingClient client;

    @Autowired
    private Tracer tracer;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        try {

            SystemUser user = client.findByUsername(username);

            List<GrantedAuthority> authority = user.getRoles().stream()
                    .map(role -> new SimpleGrantedAuthority(role.getName()))
                    .peek(auth -> log.info("User " + username + " | Role " + auth.getAuthority()))
                    .collect(Collectors.toList());

            return new User(user.getUsername(), user.getPassword(), user.getEnabled(), true, true, true, authority);

        } catch (FeignException e) {
            tracer.currentSpan().tag("error.msg", "Error, no existe usuario:" + username);
            throw new UsernameNotFoundException("Error, no existe usuario:" + username);
        }
    }

    @Override
    public SystemUser findByUsername(String username) {
        return client.findByUsername(username);
    }

    @Override
    public SystemUser update(SystemUser user, Long id) {
        return client.update(user, id);
    }

}

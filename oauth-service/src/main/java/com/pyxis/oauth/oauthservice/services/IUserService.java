package com.pyxis.oauth.oauthservice.services;

import com.pyxis.common.users.commonuser.models.entity.SystemUser;

public interface IUserService {
    
    public SystemUser findByUsername(String username);

    public SystemUser update(SystemUser user, Long id);
}

package com.pyxis.products.products.controllers;

import java.util.List;
import java.util.stream.Collectors;

import com.pyxis.common.common.models.entity.Product;
import com.pyxis.products.products.models.service.IProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    // @Autowired
    // private Environment env;

    @Value("${server.port}")
    private Integer port;

    @Autowired
    private IProductService productService;

    @GetMapping("/list")
    public List<Product> list() {
        return productService.findAll().stream().map(product -> {
            // product.setPort(Integer.parseInt(env.getProperty("local.server.port")));
            product.setPort(port);
            return product;
        }).collect(Collectors.toList());
    }

    @GetMapping("/detail/{id}")
    public Product detail(@PathVariable Long id) throws Exception {
        Product product = productService.findById(id);
        // product.setPort(Integer.parseInt(env.getProperty("local.server.port")));
        product.setPort(port);

        // if (1<2)
        // throw new Exception("error a proposito!");

        // try {
        //     Thread.sleep(5000L);
        // } catch (Exception e) {
        //     e.printStackTrace();
        // }

        return product;
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Product create(@RequestBody Product product){
        return productService.save(product);
    }

    @PutMapping("/edit/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Product edit(@RequestBody Product product, @PathVariable Long id){
        Product productStored = productService.findById(id);

        productStored.setName(product.getName());
        productStored.setPrice(product.getPrice());

        return productService.save(productStored);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        productService.deleteById(id);
    }


}

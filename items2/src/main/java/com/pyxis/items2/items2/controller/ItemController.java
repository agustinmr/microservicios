package com.pyxis.items2.items2.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.pyxis.common.common.models.entity.Product;
import com.pyxis.items2.items2.models.Item;
import com.pyxis.items2.items2.models.service.ItemService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class ItemController {

    private static Logger log = LoggerFactory.getLogger(ItemController.class);
    
    @Autowired
    private Environment env;

    @Autowired
    @Qualifier("serviceFeign")
    private ItemService itemService;

    @Value("${config.text}")
    private String text;
 
    @GetMapping("/list")
    public List<Item> lista(){
        return itemService.findAll();
    }

    @HystrixCommand(fallbackMethod = "metodoAlternativo")
    @GetMapping("/detail/{id}/cant/{cant}")
    public Item detail(@PathVariable Long id, @PathVariable Integer cant){
        return itemService.findById(id, cant);
    }

    public Item metodoAlternativo(Long id, Integer cant){
        return new Item();
    }

    @GetMapping("/get-config")
    public ResponseEntity<?> getconfig(@Value("${server.port}") String port){
        Map<String,String> json = new HashMap<>();
        json.put("text", text);
        json.put("port", port);
        log.info("Texto"+text);

        if(env.getActiveProfiles().length>0 && env.getActiveProfiles()[0].equals("dev"))
            json.put("autor", env.getProperty("config.autor"));

        return new ResponseEntity<Map<String,String>>(json,HttpStatus.OK);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Product create(@RequestBody Product product){
        return itemService.save(product);
    }
    
    @PutMapping("/edit/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Product update(@RequestBody Product product, @PathVariable Long id){
        return itemService.save(product);
    }
    
    @PutMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        itemService.delete(id);
    } 
}

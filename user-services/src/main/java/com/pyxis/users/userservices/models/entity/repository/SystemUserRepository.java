package com.pyxis.users.userservices.models.entity.repository;

import com.pyxis.common.users.commonuser.models.entity.SystemUser;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "user")
public interface SystemUserRepository extends PagingAndSortingRepository<SystemUser,Long>{
    
    @RestResource(path = "find-user")
    public SystemUser findByUsername(@Param("username") String username);

    @Query("select u from SystemUser u where u.username=?1")
    public SystemUser getThisUserByUsername(String username);

    // @Query("select u from SystemUser u where u.username=?1")
    // public SystemUser findByUsername2(String user);
}

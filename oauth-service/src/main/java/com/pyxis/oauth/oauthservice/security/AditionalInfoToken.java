package com.pyxis.oauth.oauthservice.security;

import java.util.HashMap;
import java.util.Map;

import com.pyxis.common.users.commonuser.models.entity.SystemUser;
import com.pyxis.oauth.oauthservice.services.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

@Component
public class AditionalInfoToken implements TokenEnhancer{

    @Autowired
    private IUserService userService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Map<String,Object> info = new HashMap<String, Object>();

        SystemUser user = userService.findByUsername(authentication.getName());

        info.put("name", user.getName());
        info.put("email", user.getEmail());
        info.put("lastname", user.getLastname());

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);;

        return accessToken;
    }
    
}

package com.pyxis.users.userservices;

import com.pyxis.common.users.commonuser.models.entity.Role;
import com.pyxis.common.users.commonuser.models.entity.SystemUser;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer{

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        
        config.exposeIdsFor(SystemUser.class,Role.class);
    }
    
}

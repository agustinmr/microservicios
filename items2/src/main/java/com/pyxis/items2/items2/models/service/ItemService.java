package com.pyxis.items2.items2.models.service;

import java.util.List;

import com.pyxis.items2.items2.models.Item;
import com.pyxis.common.common.models.entity.Product;

public interface ItemService {
    
    public List<Item> findAll();
    public Item findById(Long id, Integer cant);
    
    public Product save(Product product);

    public Product update(Product product, Long id);

    public void delete(Long id);
}

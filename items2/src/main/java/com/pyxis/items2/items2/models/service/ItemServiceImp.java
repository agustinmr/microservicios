package com.pyxis.items2.items2.models.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.pyxis.items2.items2.models.Item;
import com.pyxis.common.common.models.entity.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service("serviceRestTemp")
public class ItemServiceImp implements ItemService{
    
    @Autowired
    private RestTemplate clientRest;

    @Override
    public List<Item> findAll() {
        List<Product> products = Arrays.asList(clientRest.getForObject("http://product-service/list", Product[].class));

        return products.stream().map(p -> new Item(p,1)).collect(Collectors.toList());
    }

    @Override
    public Item findById(Long id, Integer cant) {
        Map<String,String> pathVars = new HashMap<String,String>();
        pathVars.put("id", id.toString());

        Product product = clientRest.getForObject("http://product-service/detail/{id}", Product.class, pathVars);
        return new Item(product,cant);
    }

    @Override
    public Product save(Product product) {
        HttpEntity<Product> body = new HttpEntity<Product>(product);
        
        ResponseEntity<Product> response =clientRest.exchange("http://product-service/create", HttpMethod.POST, body, Product.class);
        Product productResponse = response.getBody();
        return productResponse;
    }

    @Override
    public Product update(Product product, Long id) {
        HttpEntity<Product> body = new HttpEntity<Product>(product);
        Map<String,String> pathVars = new HashMap<String,String>();
        pathVars.put("id", id.toString());

        ResponseEntity<Product> response =clientRest.exchange("http://product-service/edit/{id}", HttpMethod.PUT, body, Product.class,pathVars);
        Product productResponse = response.getBody();
        return productResponse;
    }

    @Override
    public void delete(Long id) {
        Map<String,String> pathVars = new HashMap<String,String>();
        pathVars.put("id", id.toString());
        clientRest.delete("http://product-service/delete/{id}", pathVars);        
    }
}

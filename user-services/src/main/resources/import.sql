INSERT INTO system_users (username,password,enabled,name,lastname,email) VALUES ('Jhonny82','$2a$10$0QlZlniN7/vr8LRc4TuQAesn.2bUsi71hGtP8FknqIY5uzxIl4eBm',TRUE,'Jhonny','Crazy People','jhonny@mail.com');
INSERT INTO system_users (username,password,enabled,name,lastname,email) VALUES ('Jhonny83','$2a$10$MmXYZJdKsj/q28zFFJLevOXDPOkLvQZLU7xowdD.IZSBjmmUK6Q6y',TRUE,'Jhonny2','Crazy People2','jhonny2@mail.com');
-- (id , email , enabled , lastname , name , password , username , primary key (id))
--INSERT INTO `users` (user_name, password, enabled, name, last_name, email) VALUES ('teo20','1234',1, 'Teo', 'Diaz Sanz','teoo@gmail.com');


-- INSERT INTO `users` (username,password,enabled,name,lastname,email) VALUES ('Jhonny83','password',1,'Jhonny2','Crazy People2','jhonny2@mail.com');

INSERT INTO roles (name) VALUES ('ROLE_USER');
INSERT INTO roles (name) VALUES ('ROLE_ADMIN');
 
INSERT INTO system_users_roles (system_user_id, roles_id) VALUES (1, 1);
INSERT INTO system_users_roles (system_user_id, roles_id) VALUES (2, 2);
INSERT INTO system_users_roles (system_user_id, roles_id) VALUES (2, 1);
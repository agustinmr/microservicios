package com.pyxis.oauth.oauthservice.clients;

import com.pyxis.common.users.commonuser.models.entity.SystemUser;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "user-service")
public interface UserFeingClient {
    
    @GetMapping("/user/search/find-user")
    public SystemUser findByUsername(@RequestParam String username);

    @PutMapping("/user/{id}")
    public SystemUser update(@RequestBody SystemUser user, @PathVariable Long id);
}

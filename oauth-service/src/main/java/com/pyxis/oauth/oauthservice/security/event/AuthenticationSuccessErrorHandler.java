package com.pyxis.oauth.oauthservice.security.event;

import com.pyxis.common.users.commonuser.models.entity.SystemUser;
import com.pyxis.oauth.oauthservice.services.IUserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import brave.Tracer;
import feign.FeignException;

@Component
public class AuthenticationSuccessErrorHandler implements AuthenticationEventPublisher {

    @Autowired
    private IUserService userService;

    @Autowired
    private Tracer tracer;

    private Logger log = LoggerFactory.getLogger(AuthenticationSuccessErrorHandler.class);

    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        log.info("Succes Login: " + userDetails.getUsername());
        tracer.currentSpan().tag("succes.login", "Succes Login: " + userDetails.getUsername());

        SystemUser user = userService.findByUsername(authentication.getName());
        if (user.getTryNumber() != null && user.getTryNumber() >0 ) {
            user.setTryNumber(0);
            log.info("set 0 in login tries for user-> " + userDetails.getUsername());
            userService.update(user, user.getId());
        }
    }

    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        log.error("Login Error Exception: " + exception.getMessage());
        tracer.currentSpan().tag("error.msg","Login Error Exception: " + exception.getMessage());

        try {
            SystemUser user = userService.findByUsername(authentication.getName());
            if (user.getTryNumber() == null)
                user.setTryNumber(0);

            user.setTryNumber(user.getTryNumber() + 1);

            log.info("User: " + user.getUsername() + " has " + user.getTryNumber() + " login tries");

            if (user.getTryNumber() >= 3)
                user.setEnabled(false);

            log.info("User: " + user.getUsername() + " has enable? " + user.getEnabled());

            userService.update(user, user.getId());

        } catch (FeignException e) {
            log.error("Login Error Exception: User " + authentication.getName() + " not found");
            tracer.currentSpan().tag("error.msg","Login Error Exception: User " + authentication.getName() + " not found");
        }
    }

}

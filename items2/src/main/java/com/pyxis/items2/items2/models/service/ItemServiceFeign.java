package com.pyxis.items2.items2.models.service;

import java.util.List;
import java.util.stream.Collectors;


import com.pyxis.items2.items2.clients.ProductClientRest;
import com.pyxis.items2.items2.models.Item;
import com.pyxis.common.common.models.entity.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("serviceFeign")
public class ItemServiceFeign implements ItemService{
    
    @Autowired
    private ProductClientRest clientFeign;

    @Override
    public List<Item> findAll() {

        return clientFeign.list().stream().map(p -> new Item(p,1)).collect(Collectors.toList());
    }

    @Override
    public Item findById(Long id, Integer cant) {
        return new Item(clientFeign.detail(id+1), cant);
    }

    @Override
    public Product save(Product product) {
        return clientFeign.create(product);
    }

    @Override
    public Product update(Product product, Long id) {
        return clientFeign.update(product, id);
    }

    @Override
    public void delete(Long id) {
        clientFeign.delete(id);
        
    }
    
}

INSERT INTO product (name, price, created_at) VALUES ('Arena para gato', 220, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Masajeador para gato', 420, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Caja para gato', 90, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Comida para gato', 900, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Cerveza para gato', 760, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Cuna para gato', 150, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Manta para gato', 670, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Chapita para gato', 2150, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Correa para gato', 254, NOW());
INSERT INTO product (name, price, created_at) VALUES ('Altar para gato', 9999.99, NOW());
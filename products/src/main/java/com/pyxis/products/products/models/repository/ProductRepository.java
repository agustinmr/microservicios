package com.pyxis.products.products.models.repository;

import com.pyxis.common.common.models.entity.Product;

import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product,Long>{
    
}
